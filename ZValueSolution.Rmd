### **_Solution_**

We have mean:
`r latex_display("
                                \\mu=<%mu%>
")`

We have standard deviation:
`r latex_display("
                                \\sigma=<%sigma%>
")`

We have $x$-value:
`r latex_display("
                                x=<%x%>
")`


From these we can find the value of z as follows:

`r latex_display("
                        z=\\frac{x-\\mu}{\\sigma}
                         =\\frac{<%x%> - <%mu%>}{<%sigma%>}
                         =\\frac{<%numer%>}{<%sigma%>}
                         =<% round(z,2)%>
")`
