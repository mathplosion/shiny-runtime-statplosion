
split_str_by_index <- function(target, index) {
  index <- sort(index)
  substr(rep(target, length(index) + 1),
         start = c(1, index),
         stop = c(index -1, nchar(target)))
}

what<-c("mu","sigma")
after<-c(10,17)

df<-data.frame(what=what, 
               after=after)

print(df)

str<-"Z<-(-2*mu)+sigma+4"
print(str)
split<-split_str_by_index(str, df$after)
print(split)
newstr<-paste(split, collapse="()")
print(newstr)
