library(formula.tools)

get_lhs_rhs<-function(expr) {
    my_call<-expr[[1]]
	lhs<-deparse(my_call[[2]])
	rhs<-deparse(my_call[[3]])
    parts<-c()
    parts$lhs<-lhs
    parts$rhs<-rhs
    return (parts) 
}

line<-"plot(x,y)"
expr<-parse(text=line)
print(expr)
all.vars(expr)
my_call<-expr[[1]]
lhs<-my_call[[2]]
rhs<-my_call[[3]]
print("my_call:")
print(my_call)
print("lhs:")
print(lhs)
print("rhs:")
print(rhs)
