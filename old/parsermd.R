library(parsermd)


ast = parse_rmd("Problem.Rmd")

yaml<-ast[[1]]

str(yaml$params)

str(yaml$params$num$value)

ast[[1]]$params$num$value = 200 

rmd_text<-as_document(ast)

str(rmd_text)

as_document(ast, collapse="\n") %>%
    cat(file="blah.Rmd", sep="\n")


knitr::opts_chunk$set(echo = FALSE)
parsermd::render(ast, 
               output_format="html_fragment",
               output_options=list(self_contained=T),
               envir=new.env())
