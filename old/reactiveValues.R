library(shiny)
library(rlang)

reactiveConsole(TRUE)
#options("keep.source"=TRUE)

params<-reactiveValues(x=3,num=5)

make_reactive<-function(rhs) {
   original_rhs<-rhs
   parsed<-parse(text=rhs)
   vars<-all.vars(parsed)
   vars<-vars[!vars %in% c("params")]
   if(length(vars) > 0)
       for(i in 1:length(vars)) {
         rhs<-gsub(vars[i],paste0(vars[i],"()"), rhs)
       }
   return (rhs)
}

get_lhs_rhs<-function(expr) {
    my_call<-expr[[1]]
	lhs<-deparse(my_call[[2]])
	rhs<-deparse(my_call[[3]])
    parts<-c()
    parts$lhs<-lhs
    parts$rhs<-rhs
    return (parts) 
}

lines<-c("pp<-params[['x']]+params[['num']]",
         "q<-2*pp+params[['x']]",
         "sigma<-c(2*q,params[['num']])",
         "mu<-sqrt(sigma[1])",
         "nu<-mean(sigma)")

print("original lines:")
for(i in 1:length(lines))
    print(lines[i])

cat("\n\n")
print("reactive lines:")
for(i in 1:length(lines)) {
    sides<-get_lhs_rhs(parse(text=lines[i]))
    lhs<-sides$lhs
    rhs<-sides$rhs
    rhs<-make_reactive(rhs)
    print(paste0(lhs, "<-", rhs))
}

assignments<-lapply(lines, function(line) {
    sides<-get_lhs_rhs(parse(text=line))
    lhs<-sides$lhs
    rhs<-sides$rhs
    rhs<-make_reactive(rhs)
    assign(lhs, 
           reactive({
                eval(parse(text=rhs))
           }), 
           envir=.GlobalEnv)
})

observe({
	cat("observer 1", "\n\n")
    cat("x:", params$x, " num:", params$num, "\n")
    cat("pp:", pp(), " q:", q(), "sigma:", sigma(),"\n")
    cat("mu:", mu(), " nu:", nu(), "\n")
})

params$x<-9

reactiveConsole(FALSE)
