library(shiny)
# library(bslib)
library(parsermd)
library(shinyjs)

run_mathjax<-function(...) {
       tagList(
        ...,
        tags$script(HTML('MathJax.typeset()'))
      )
}

ui<-navbarPage("Statplosion", id = "tabs", collapsible=T,
      # theme=bs_theme(version=5, bootswatch="zephyr"),
      tabPanel("Editor",
        fluidRow(
              column(width=6, id="main", 
                     div(style="float:right",
                        # actionButton("toggleRMarkdown", 
                        #              "Toggle RMarkdown",
                        #               class="btn-xs"
                        #              ),
                        actionButton("saveParams", 
                                     "Save Params",
                                      class="btn-xs btn-success"
                                     )
                         ),
                    HTML("<h4> Params </h4>"),
                    uiOutput("params"),
                    HTML("<h4> Preview </h4>"),
                    htmlOutput("doc_out"),
              ),
              column(width=6, id="spcol", 
                     div(style="float:right",
                    actionButton("saveRmarkdown", 
                                 "Save RMarkdown", 
                                 class="btn-xs btn-success"),
                         ),
                  tabsetPanel(id = "tabset",
                    tabPanel("Problem", 
                              textAreaInput("problem", 
                                            label="", 
                                            rows=50,
                                            width="100%", 
                                            value=NULL,
                                            resize="vertical")
                            ),
                    tabPanel("Question", 
                              textAreaInput("Question", 
                                            label="", 
                                            rows=50,
                                            width="100%", 
                                            resize="vertical")
                            ),
                    tabPanel("Solution", 
                              textAreaInput("Solution", 
                                            label="", 
                                            rows=50,
                                            width="100%", 
                                            resize="vertical")
                            )
                  )
              ),
              tags$head(
HTML("<script> MathJax = {tex:{inlineMath:[['$', '$'],['\\\\(','\\\\)']]}}; </script>"),
HTML("<script src='https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js'></script>"),
HTML("<style> .recalculating { opacity: 1.0 !important; } </style>"),
HTML("<style> .navbar-nav > li > a, .navbar-brand { padding-top:5px !important; padding-bottom:0 !important; height: 30px; } .navbar {min-height:30px !important; margin-bottom:3px; !important}")
              ),
              shinyjs::useShinyjs()
          )
   ),
  tabPanel("Download", 
              textInput("prefix", "Prefix", value = "Problem"),
              downloadButton("download_html", "Download Html"), 
              br(),
              br(),
              downloadButton("download_rmd", "Download RMarkdown")
           ),
actionButton("toggleRMarkdown", 
             "Toggle RMarkdown",
              class="btn-xs"
             ),
)

#ui <- htmlTemplate("www/index.html")

server <- function(input, output, session) {

    rmd_ast<-reactiveVal()

    filename<-reactiveVal("Problem.Rmd")

    observeEvent(filename(), {
       #read the new rmd file
       ast<-parse_rmd(filename())

       #set the new ast rmd 
       rmd_ast(ast)
    })

    params<-reactiveValues(values=list())

    # These are the current yaml params
    yaml_params<-reactive({
      cat(file=stderr(), "yaml_params reactive", "\n")  
      yaml<-rmd_ast()[[1]]
      yaml$params
    })

    # rmd<-reactiveFileReader(500, session, filename, identity)

    output$params<-renderUI({
      cat(file=stderr(), "renderUI called: making params UI", "\n")  

      yml_params<-yaml_params()
      names<-names(yml_params)

        components<-lapply(names, function(name) {
            param<-yml_params[[name]]
            input<-param$input
            switch(input, 
                "slider" = sliderInput(name, 
                            label = param$label, 
                            min = param$min, 
                            max = param$max, 
                            step = param$step, 
                            value = param$value),
                "numeric" = numericInput(name, 
                            label = param$label, 
                            value = param$value),
                "text" = textInput(name, 
                            label = param$label, 
                            value = param$value)
            )
        })
        run_mathjax(tagList(components))
    })

  observeEvent(filename(),{
      cat(file=stderr(), "observeEvent: filename() ", "\n")  
      lines <- xfun::read_utf8(filename())
      text <- paste0(lines, collapse="\n")

      cat(file=stderr(), "updateTextAreaInput ", "\n")  
      updateTextAreaInput(session, "problem", value=text)
  })

  doc<-reactive({
      cat(file=stderr(), "doc reactive", "\n")  

      knitr::opts_chunk$set(echo = FALSE)
      knitr::opts_knit$set(progress = FALSE)

      req(length(params$values)!=0)

      cat(file=stderr(), "params values are", "\n")  
      print(params$values)

      html_file<-parsermd::render(rmd_ast(), 
                                   output_format="html_fragment",
                                   output_options=list(self_contained=T),
                                   envir=new.env(),
                                   params=params$values)

      html <- xfun::read_utf8(html_file)

  })

  observeEvent(yaml_params(), {
      cat(file=stderr(), "observeEvent yaml_params", "\n")  
      yml_params<-yaml_params()
      names<-names(yml_params)

      params$values<-list()
      lapply(names, function(name) {
          # Set the params values directly from yaml since inputs are not set correctly yet.
          params$values[[name]]<-yml_params[[name]]$value

          # Also set up any new input observers
          if(is.null(input[[name]])) {

               cat(file=stderr(), "creating observeEvent for" , name, "\n")  

               observeEvent(input[[name]], { 
                    cat(file=stderr(), "observe input$", name, "\n")  
                    if(!is.null(params$values[[name]])) {
                        cat(file=stderr(), "setting param from input", name, "\n")  
                        cat(file=stderr(), "param", params$values[[name]], "\n")  
                        cat(file=stderr(), "input", input[[name]], "\n")  
                        if(params$values[[name]] != input[[name]]) {
                            shinyjs::addClass("saveParams", "btn-warning")
                            params$values[[name]]<-input[[name]]
                        }
                    }
               })
          }  
      })
  })

    output$download_rmd<- downloadHandler(
        # this gives the Rmd filename
        filename = function() {
            return (paste0(input$prefix, ".Rmd"))
        },

        # this saves the Rmd file
        content = function(file) {
          # lines <- xfun::read_utf8("test.Rmd")
          lines<-as_document(rmd_ast())
          text <- paste0(lines, collapse="\n")
          lines <- xfun::write_utf8(lines, file)

        }
    )

    output$download_html<- downloadHandler(
        # this gives the filename
        filename = function() {
            return (paste0(input$prefix, ".html"))
        },

        # save the content to file argument 
        content = function(file) {
          cat(file=stderr(), "content:file is", file, "\n")  

           current_dir<-normalizePath(dirname("."))
           math_config_path<- paste0(current_dir, "/math-config.html")

           # cat(file=stderr(), "path_to", path_to_math_config, "\n")  

           doc<-rmarkdown::html_document(self_contained=T,
           mathjax="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js",
           includes=rmarkdown::includes(in_header = math_config_path))

           knitr::opts_chunk$set(echo = FALSE)
           tempfilename<-tempfile()
           cat(file=stderr(), "temp file name", tempfilename, "\n")  
           base<-basename(tempfilename)
           cat(file=stderr(), "base file name", base, "\n")  
           html_file<-parsermd::render(rmd_ast(), 
                                output_format = doc,
                                envir = new.env(parent = globalenv()))

          cat(file=stderr(), "html_file is", html_file, "\n")  

          lines <- xfun::read_utf8(html_file)
          xfun::write_utf8(lines, file) 

          unlink(paste0(tempfilename,".html"))

        }
    )

  observeEvent(input$saveParams, { 
      cat(file=stderr(), "observeEvent saveParams", "\n")  
      shinyjs::removeClass("saveParams", "btn-warning")
      cat(file=stderr(), "input$num", input$num, "\n")  

      ast<-rmd_ast()
      ast[[1]]$params$num$value<-input$num
      rmd_ast(ast)

      cat(file=stderr(), "yaml params after", "\n")  
      print(rmd_ast()[[1]]$params)

      text<-as_document(rmd_ast(), collapse="\n")
      updateTextAreaInput(session, "problem", value=text)
    })


  observeEvent(input$toggleRMarkdown, {
    cat(file=stderr(), "observeEvent input$toggleRMarkdown", "\n")  
    shinyjs::toggle(id = "spcol")
    shinyjs::toggleClass("main", "col-sm-12")
    shinyjs::toggleClass("main", "col-sm-6")
  })

  observeEvent(input$saveRmarkdown, {
    cat(file=stderr(), "observeEvent input$saveRmarkdown", "\n")  
    cat(file=stderr(), "observeEvent tab: ", input$tabset, "\n")  

    shinyjs::removeClass("saveRmarkdown", "btn-warning")

    if(input$tabset == "Problem") { 

      cat(file=stderr(), "setting the new value into rmd_ast", "\n")  

      # set the rmd to NULL so that rmd_ast marks as being changed
      rmd_ast(NULL)

      ast<-parse_rmd(input$problem)
      rmd_ast(ast)
    }
    else
        cat(file=stderr(), "no such tabset", "\n")  

  })

  output$doc_out <- renderUI({
      cat(file=stderr(), "renderUI doc_out", "\n")  
      run_mathjax(HTML(doc()))
  })
}

shinyApp(ui=ui, server=server) 
