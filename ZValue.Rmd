---
runtime: shiny
pagetitle: "Z Value"
title: "Finding a $z$-value"
question: "ZValueQuestion.Rmd" 
solution: "ZValueSolution.Rmd" 
show_question: yes 
show_solution: yes 
params:
  mu:
    label: "\\large{\\mu}"
    value: 3
    input: numeric 
  sigma:
    label: "\\large{\\sigma}"
    value: 2
    input: numeric 
  x:
    label: "\\large{x}"
    value: 8
    input: numeric 
output:
  html_document: 
    math_method: r-katex
---

```{r setup, echo=F}
library(rmarkdown)
library(editableRmd)

question<-rmarkdown::metadata$question
solution<-rmarkdown::metadata$solution
show_question<-rmarkdown::metadata$show_question
show_solution<-rmarkdown::metadata$show_solution

editableRmd::setRuntime(rmarkdown::metadata$runtime)
runtimeShiny<-editableRmd::isShinyRuntime()

# add this if runtime shiny
if(runtimeShiny) {
    xlabel<-rmarkdown::metadata$params$x$label
    mulabel<-rmarkdown::metadata$params$mu$label
    sigmalabel<-rmarkdown::metadata$params$sigma$label
    tagList(
        numericInput("mu", 
                     HTML(katex::katex_html(mulabel, displayMode=F)), 
                     params$mu),
        numericInput("sigma", 
                     HTML(katex::katex_html(sigmalabel, displayMode=F)), 
                     params$sigma),
        numericInput("x", 
                     HTML(katex::katex_html(xlabel, displayMode=F)), 
                     params$x)
    )
}
```

```{r params, echo=F}
if(!runtimeShiny) {
    x<-params$x
    mu<-params$mu
    sigma<-params$sigma
} else {
    # replace the above with this if runtime shiny...
    x<-reactive(input$x)
    mu<-reactive(input$mu)
    sigma<-reactive(input$sigma)
}
```

```{r computed, echo=F}
if(!runtimeShiny) {
    numer<-(x-mu)
    z<-numer/sigma
} else {
    # replace the above with this if runtime shiny...
    numer<-reactive({
       for(var in ls(paramEnv)){
           val <- do.call(var, list())
           assign(var, val) 
       }
       (x-mu)
    })
    z<-reactive({
       for(var in ls(paramEnv)){
           val <- do.call(var, list())
           assign(var, val) 
       }
       numer<-numer()
       (numer/sigma) 
    })
}
```

```{r echo=F, eval=T}
paramEnv<-new.env() 
paramEnv$x <- x 
paramEnv$mu <- mu 
paramEnv$sigma <- sigma 
editableRmd::setParamEnv(paramEnv)
computedEnv<-new.env()
computedEnv$numer <- numer 
computedEnv$z <- z 
editableRmd::setComputedEnv(computedEnv)
```

```{r, child=question, eval=show_question}
```
```{r, child=solution, eval=show_solution}
```
