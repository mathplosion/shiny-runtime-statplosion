
process_params_chunk<-function(lhs, rhs) {

    lines<-c()

    for(i in 1:length(lhs)) {
        if(startsWith(rhs[i], "param$")) {
           line<-paste0(lhs[i], 
                        "<-", 
                        "reactive(", 
                        "input$", 
                        lhs[i], 
                        ")")    
           lines<-c(lines, line)
        }
        else {
           index<-match(lhs[i], lhs)
           prev_lhs<-lhs[1:(index-1)]
           assignments<-""
           for(j in 1:length(prev_lhs)) {
                name<-prev_lhs[j]
                assignment<-paste0(name, 
                                  "<-", 
                                  name, 
                                  "()", 
                                  ";")
                assignments<-paste0(assignments, assignment)
            }
           line<-paste0(lhs[i], 
                        "<-", 
                        "reactive({", 
                        assignments,
                        rhs[i], 
                          "})")
           lines<-c(lines, line)
        }
    }

    return (lines)
}


lhs<-c("x", "d") 
rhs<-c("param$x", "2*x")
lines<-process_params_chunk(lhs, rhs) 

print(lines)
