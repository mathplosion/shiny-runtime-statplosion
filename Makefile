SHELL:=/bin/bash

SOURCES = ZValue.Rmd Histogram.Rmd
HTML_FILES = $(SOURCES:%.Rmd=%.html)

all : shinyrun

html: $(HTML_FILES)

clean :
	@echo Removing html, folders, files...	
	rm -f $(HTML_FILES) README.html
	rm -rf *_files

readme:
	@Rscript -e  'library(rmarkdown)' \
         -e 'render("README.md", "html_document", output_options=list(self_contained=F))'

shiny:
	@Rscript run_app.R

editableRmd:
	@Rscript build_editableRmd.R

docker: 
	docker build -t editable-rmarkdown-problems .

run: docker 
	docker run -p 8080:8080 editable-rmarkdown-problems

.PHONY: all clean
