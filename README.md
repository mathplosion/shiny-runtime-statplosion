# Editable RMarkdown Problems

- [Simple](Simple.Rmd) This example is very simple
- [Histogram](Histogram.Rmd) This example shows choosing random numbers for a histogram
- [Problem](Problem.Rmd) This example shows lots of equation examples
- [ZValue](ZValue.Rmd) This example shows finding a z-value 

## What Is This? 

Included here are examples (using RMarkdown) of creating problems with questions and solutions for elementary statistics problems.

## Problems as Parametrized RMarkdown

The idea is that of a parametrized problem similar to [parametrized reports](https://bookdown.org/yihui/rmarkdown/parameterized-reports.html), also described [here](https://garrettgman.github.io/rmarkdown/developer_parameterized_reports.html#overview). 

The parametrized documents are created using the [rmarkdown](https://bookdown.org/yihui/rmarkdown/) format.   

There are several parameters for each problem that are passed in as part of the Yaml header of the document, then `rmarkdown::render` renders the question and solution based on the type of problem and the parameters. 

See Simple.Rmd for a simple example of this, which passes one parameter, an $x$. The problem then defines a _computed_ value $d=2x$. The params fields are passed in the yaml header of the RMarkdown file, the computed value d is computed from that value.

When you put in the option **runtime:shiny** to the header of the document, you get an editable version of the document.

## Making It Interactive 

#### Interactive or Shiny Documents 

It is possible to use a shiny for setting the parameters for the parametrized documents linked above Typically you use the params=\"ask\" argument for this as described [here](https://bookdown.org/yihui/rmarkdown/params-knit.html#the-interactive-user-interface)). After you set the parameters the document is rendered using those parameters.

It is also possible to use shiny in the rmarkdown document itself for various kinds of interactive displays. This has been described in [Shiny Documents](https://bookdown.org/yihui/rmarkdown/interactive-documents.html#intro-shiny) and also [Interactive documents](https://shiny.rstudio.com/articles/interactive-docs.html). 

Our approach is a combination of the above, where we present shiny style UI (like for setting the params above) but we do this as part of the RMarkdown document which allows seeing the result of the parameter setting changes right away on the rendered document. 

This approach requires some care to the way we handle inline R code since Shiny does not always work well with some approaches for inline code. Basically all inline R code expressions have to be wrapped in something like `renderUI` in order to make them reactive, and we have to be careful to not embed inline code expressions within Latex expressions since Shiny does not work well for that. We give some examples below.  

#### Installation 


